<?php get_header(); ?>

	<div id="content" class=" left clearfix">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php remove_filter ('the_content', 'wpautop'); ?>
	
		<?php the_content(); ?>
	</div><!-- / #content end -->
<?php endwhile; else: ?>
	 <?php include("404.php"); ?>
<?php endif; ?>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
