
		<?php while (have_posts()) : the_post(); ?>
		<div class="article single">
			<div class="h2Sec clearfix">
				<span class="date"><?php the_time('Y.m.d'); ?></span>
				<h2><?php the_title(); ?></h2>
			</div>
            <div class="case_area">
           <div class="case_main">
           <img src="<?php the_field('main'); ?>"  width="600"/>
           </div>
           <div class="case_content">
			<p><?php the_content(); ?></p>
            </div>
            
            <div class="case_ex">
            <table cellpadding="0" cellspacing="0">
            <tr>
            <td>
			<a href="<?php the_field('photo01'); ?>" width="400"><img src="<?php the_field('photo01'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt01'); ?></p></div>
            </td>
            <td>
			<a href="<?php the_field('photo02'); ?>" width="400"><img src="<?php the_field('photo02'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt02'); ?></p></div>
            </td>
            </tr>
             <tr>
            <td>
			<a href="<?php the_field('photo03'); ?>" width="400"><img src="<?php the_field('photo03'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt03'); ?></p></div>
            </td>
            <td>
			<a href="<?php the_field('photo04'); ?>" width="400"><img src="<?php the_field('photo04'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt04'); ?></p></div>
            </td>
            </tr>
             <tr>
            <td>
			<a href="<?php the_field('photo05'); ?>" width="400"><img src="<?php the_field('photo05'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt05'); ?></p></div>
            </td>
            <td>
			<a href="<?php the_field('photo06'); ?>" width="400"><img src="<?php the_field('photo06'); ?>"  width="250"/></a>
            <div class="txt_box"><p><?php the_field('txt06'); ?></p></div>
            </td>
            </tr>
            </table>
            
            
            <br clear="all" />
           </ul>
           </div>
           
            
            
            
           </div>
		</div><!-- /.article -->
		<?php endwhile; ?>
	