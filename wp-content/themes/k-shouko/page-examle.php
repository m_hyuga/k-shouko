<?php
/*
Template Name: example
*/
?>
<?php get_header(); ?>

	<div id="content" class=" left clearfix">
		<div class="h2SecWrapper">
<div class="h2Sec">
<h2><?php the_title(); ?></h2>
</div>
</div>
			
			
		<?php $paged = get_query_var('paged'); ?>
		<?php query_posts("posts_per_page=12&paged=".$paged) ; ?>
        <div class="case_list">
		<ul>
		<?php while( have_posts() ) : the_post(); ?>
        <?php if ( ! in_category('4')) continue; ?>
        
			<li><a href="<?php the_permalink(); ?>">
            <?php
	$attachment_id = get_field('thumb');

	$image = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
	$attachment = get_post( get_field('thumb') );
	$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
	$image_title = $attachment->post_title;
?>
           <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $alt; ?>" title="<?php echo $image_title; ?>" /></a>
				<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				
			</li>
		<?php endwhile; ?>
        <br clear="all" />
		</ul>
        </div>
		<?php wp_pagenavi(); ?>
		

	</div><!-- / #content end -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>