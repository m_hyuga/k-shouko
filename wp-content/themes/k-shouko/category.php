<?php get_header(); ?>

	<div id="content" class=" left clearfix">
		<?php $paged = get_query_var('paged'); ?>
		<?php query_posts("posts_per_page=10&paged=".$paged) ; ?>
		<ul class="article">
		<?php while( have_posts() ) : the_post(); ?>
			<li>
				<span class="date"><?php the_time('Y.m.d'); ?></span>
				<h2>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php wp_pagenavi(); ?>
	</div><!-- / #content end -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>