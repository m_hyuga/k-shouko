<?php
/*
Template Name: news
*/
?>
<?php get_header(); ?>

	<div id="content" class=" left clearfix">
		<?php if (have_posts()) : query_posts('showposts=3'); ?>
		<ul class="article">
		<?php while (have_posts()) : the_post(); ?>
			<li>
				<span class="date"><?php the_time('Y.m.d'); ?></span>
				<h2>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
			</li>
		<?php endwhile; ?>
		</ul><!-- /.article -->
		<?php wp_reset_query(); endif;  ?>
	</div><!-- / #content end -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>