<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="ja" lang="ja" dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?>
</title>
<meta name="keywords" content="金沢商行" />
<meta name="description" content="<?php bloginfo('description'); ?>" />
<meta name="copyright" content="Copyright (c) kanazawashoko All Rights Reseved." />
<meta name="robots" content="index,nofollow" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<?php	wp_head(); ?>
</head>
<?php if ( is_home() || is_front_page() ) : ?>
<body>
<?php elseif ( is_page() ) : ?>
<?php $ancestor = array_pop(get_post_ancestors($post->ID)); ?>
<body id="page" class="<?php echo get_page_uri( $ancestor ) ?>">
<?php else : ?>
<body id="page">
<?php endif; ?><div id="container">
	<div id="header">
		<div id="pnavArea">
			<div id="pnavAreaInner" class="clearfix">
				<ul id="pnav" class="clearfix">
					<li><a href="/sitemap">サイトマップ</a></li>
					<li><a href="/privacy">プライバシーポリシー</a></li>
				</ul>
			</div>
		</div>
		<div id="gnavAreaWrapper">
			<div id="gnavArea" class="clearfix">
				<h1 class="logo"><a href="/"><img src="/images/common/logo_kanazawa_shouko.gif" alt="金沢商行" /></a></h1>
				<ul id="gnav" class="clearfix">
					<li><a class="kngn_01" href="/company">会社案内</a></li>
					<li><a class="kngn_02" href="/service/flow">事業内容</a></li>
					<li><a class="kngn_03" href="/case">事例紹介</a></li>
					<li><a class="kngn_04" href="https://job.rikunabi.com/2016/company/top/r151300081/" target="_blank">採用情報</a></li>
					<li><a class="kngn_05" href="/contact">お問い合わせ・資料請求</a></li>
				</ul>
			</div><!-- /#gnavArea -->
		</div><!-- /#gnavAreaWrapper -->
        <div id="subnavAreaWrapper">
            <div id="subnavArea">
                <ul>
                    <li><a href="/service/eco-product/">システムバス</a></li>
                    <li><a href="/service/eco-product/toilet/">トイレ</a></li>
                    <li><a href="/service/eco-product/kitchen/">システムキッチン</a></li>
                    <li><a href="/service/eco-product/senmen/">洗面化粧台</a></li>
                    <li><a href="/service/eco-product/kyutouki/">給湯器</a></li>
                    <li><a href="/service/eco-product/alldenka/">オール電化</a></li>
                    <li><a href="/service/eco-product/kenzai/">建材商品</a></li>
                    <li><a href="/service/eco-product/exterior/">エクステリア</a></li>
                    <li><a href="/service/eco-product/sasshi/">サッシ</a></li>
                </ul>
                <br clear="all" />
            </div>
        </div>
	</div><!-- /#header -->
	<?php if ( is_home() || is_front_page() ) : ?>
	<div id="slideWrapper">
		<div id="slide">
			<img src="/images/home/001_suggestion.jpg" alt="エネルギーを「創る」「減らす」「活かす」暮らしをご提案します。" />
			<img src="/images/home/002_network.jpg" alt="最新の商品と技術力で夢をカタチに変えるネットワーク体制。" />
			<img src="/images/home/003_spatial.jpg" alt="都市空間から住空間にいたるまでプラスαのご提案。" />
		</div><!-- /#slide -->
	</div>
	<?php else : ?>
	<div id="pankuzuArea">
		<?php get_breadcrumbs(); ?>
	</div>
	<?php endif; ?>
	<div id="mainArea" class="clearfix">