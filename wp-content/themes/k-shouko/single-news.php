
		<?php while (have_posts()) : the_post(); ?>
		<div class="article single">
			<div class="h2Sec clearfix">
				<span class="date"><?php the_time('Y.m.d'); ?></span>
				<h2><?php the_title(); ?></h2>
			</div>
			<?php the_content(); ?>
		</div><!-- /.article -->
		<?php endwhile; ?>
	