<?php get_header(); ?>
		<div id="content">
			<div class="contentBlock">
				<div class="clearfix">
					<div class="titleArea clearfix">
						<h2 class="left"><img src="/images/home/h2_whats_new.gif" alt="新着情報 WHAT'S NEW" /></h2>
						<ul class="more right">
							<li>
								<a href="/news">一覧を見る</a>
							</li>
						</ul>
					</div>
				</div>
				<dl class="dateList clearfix">
               <?php
$myposts = get_posts('category=1 & showposts=4');
foreach($myposts as $post) :
?>
					<dt><?php the_time('Y.m.d'); ?></dt>
					<dd>
						<a href="<?php the_permalink(); ?>">
							<h3><?php the_title(); ?></h3>
						</a>
					</dd>
					
                    <?php endforeach; ?>
				</dl>
			</div><!-- /.contentBlock -->
			<div class="clearfix">
				<div class="contentBlock half left">
					<h2><img src="/images/home/h2_business_activity.gif" alt="事業内容 BUSINESS" /></h2>
					<a href="/service" class="blockLink">
						<img src="/images/home/img_business_activity.jpg" alt="金沢商行の事業内容" />
						<span>施工から納入まで／取扱メーカー一覧／取り扱い商品<br />セメント建材部／住宅瑕疵保険(住宅あんしん保証)</span>
					</a>
				</div><!-- ./contentBlock -->
				<div class="contentBlock half right">
					<h2><img src="/images/home/h2_case_introdution.gif" alt="事例紹介 CASE INTRODUCTION" /></h2>
					<a href="/case" class="blockLink">
						<img src="/images/home/img_case_introduction.jpg" alt="金沢商行の事例紹介" />
                         <?php
$myposts = get_posts('category=4 & showposts=1');
foreach($myposts as $post) :
?>
						<a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
					<?php endforeach; ?>
				</div><!-- ./contentBlock -->
			</div>
		</div><!-- /#content -->
		<?php get_sidebar(); ?>
<?php get_footer(); ?>