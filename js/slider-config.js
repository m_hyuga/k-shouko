(function($) {
	$(window).load(function() {
		$('div#slide').nivoSlider({
			directionNav:false,
/* 			directionNavHide: false, */
			effect:'fade',
			manualAdvance:false,
			animSpeed:500,
			controlNav:false,
			animSpeed:800,
			pauseTime:5500,
			slices:1
		});
	}); 
})(jQuery);
